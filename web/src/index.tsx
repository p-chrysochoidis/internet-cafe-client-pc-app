import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import App from './app/app.component';
import { appStore } from './app/store';
import { Environment } from './app/utils/environment';

import './index.scss';

Environment.setupDev(appStore);

ReactDOM.render((
    <Provider store={ appStore }>
        <App />
    </Provider>
), document.getElementById('root'));
