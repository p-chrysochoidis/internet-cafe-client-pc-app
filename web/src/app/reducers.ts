import { combineReducers } from 'redux';

import { ConnectionState, connectionReducer } from './connection/reducers';
import { ElectronState, electronReducer } from './electron/reducers';
import { ClientPcStateModel } from './client-pc/client-pc-state.model';
import { clientPcReducer } from './client-pc/reducers';
import { UserStateModel } from './user/user-state.model';
import { userReducer } from './user/reducers';
import { BackgroundImagesStateModel } from './background-images/state.model';
import { backgroundImagesReducer } from './background-images/reducers';

export interface AppState {
    connection: ConnectionState,
    electron: ElectronState,
    clientPc: ClientPcStateModel,
    user: UserStateModel,
    backgroundImages: BackgroundImagesStateModel,
}

export const rootReducer = combineReducers<AppState>({
    connection: connectionReducer,
    electron: electronReducer,
    clientPc: clientPcReducer,
    user: userReducer,
    backgroundImages: backgroundImagesReducer,
});