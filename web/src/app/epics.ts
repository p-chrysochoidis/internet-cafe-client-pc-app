import { combineEpics } from 'redux-observable';

import { connectionEpic } from './connection/epics';
import { clientPcEpic } from './client-pc/epics';
import { backgroundImagesEpic } from './background-images/epics';

export const rootEpic = combineEpics(connectionEpic, clientPcEpic, backgroundImagesEpic);
