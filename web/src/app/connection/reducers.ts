import { AnyAction } from 'redux';
import { handleActions } from 'redux-actions';

import { ConnectionStatus } from './connection-state';
import { connectionActions } from './constants';

export interface ConnectionState {
    connected: boolean,
    status: ConnectionStatus,
}

const emptyState: ConnectionState = {
    connected: false,
    status: ConnectionStatus.initial,
};

function stateChanged(state: ConnectionState, action: AnyAction) {
    const { status } = action.payload;
    const connected = status === ConnectionStatus.connected;
    return { ...state, status: status, connected };
}

export const connectionReducer = handleActions(
    {
        [connectionActions.statusChanged]: stateChanged,
    },
    emptyState,
);