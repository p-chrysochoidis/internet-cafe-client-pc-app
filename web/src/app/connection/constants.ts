const prefix = 'app.connection.';

export const connectionActions = {
    connectionCreated: `${prefix}connection-created`,
    statusChanged: `${prefix}status-changed`,
};