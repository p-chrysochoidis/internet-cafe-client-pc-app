import { createAction } from 'redux-actions';
import { connectionActions } from './constants';
import { ConnectionStatus } from './connection-state';

export const connectionStatusChanged = createAction(connectionActions.statusChanged, (status: ConnectionStatus) => ({
    status,
}));
export const streamConnectionCreated = createAction(connectionActions.connectionCreated);