import { MessageModel } from '../message.model';

export interface RegisterClientPcModel extends MessageModel {
    key: string;
    pcIp: string;
    pcName: string;
    pcOsArch: string;
    pcOsRelease: string;
    pcOsType: string;
    pcUserName: string;
}