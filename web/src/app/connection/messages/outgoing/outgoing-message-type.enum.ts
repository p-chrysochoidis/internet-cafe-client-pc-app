export enum OutgoingMessageTypeEnum {
    registerClientPc = 'registerClientPc',
    loadBackgroundImages = 'loadBackgroundImages',
}