import { MessageModel } from '../message.model';

export interface LoadedBackgroundImagesModel extends MessageModel {
    images: string[];
}