import { IncomingMessageTypeEnum } from './incoming/incoming-message-type.enum';
import { OutgoingMessageTypeEnum } from './outgoing/outgoing-message-type.enum';

export interface MessageModel {
    id?: string;
    type: IncomingMessageTypeEnum | OutgoingMessageTypeEnum;
}