import { combineEpics, Epic, ofType } from 'redux-observable';
import { map, mergeMapTo } from 'rxjs/operators';
import { AnyAction } from 'redux';

import { connectionStatusChanged, streamConnectionCreated } from './actions';
import { clientPcActions } from '../client-pc/constants';
import { Stream } from './stream';
import { connectionActions } from './constants';

const createStreamEpic: Epic = (action) => action.pipe(
    ofType<AnyAction, AnyAction, string>(clientPcActions.clientInfoReady),
    map(() => {
        Stream.connect();
        return streamConnectionCreated();
    })
);

const streamConnectionStatusEpic: Epic = (action) => action.pipe(
    ofType<AnyAction, AnyAction, string>(connectionActions.connectionCreated),
    mergeMapTo(Stream.status),
    map((status) => connectionStatusChanged(status))
);

export const connectionEpic = combineEpics(
    createStreamEpic,
    streamConnectionStatusEpic
);