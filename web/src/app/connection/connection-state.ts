export enum ConnectionStatus {
    initial,
    initializing,
    connected,
    disconnected,
}