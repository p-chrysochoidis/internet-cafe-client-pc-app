import io from 'socket.io-client';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import uuid from 'uuid/v4';

import { Logger } from '../utils/environment';
import { ConnectionStatus } from './connection-state';
import { MessageModel } from './messages/message.model';
import { filter, mergeMap, take, timeout } from 'rxjs/operators';

const STREAM_HOST = process.env.REACT_APP_STREAM_HOST;
const STREAM_PORT = process.env.REACT_APP_STREAM_PORT;

class StreamClass {
    private static readonly WAIT_FOR_CONNECTION_TIMEOUT: number = 10000;
    private static readonly WAIT_FOR_REPLY_TIMEOUT: number = 10000;
    private readonly streamUrl: string = `${ STREAM_HOST }:${ STREAM_PORT }`;
    private mStream: any = null;
    private mConnectionCreated: boolean = false;
    private mConnectionStatus: BehaviorSubject<ConnectionStatus> = new BehaviorSubject(ConnectionStatus.initial);
    private mIncomingMessages: Subject<MessageModel> = new Subject<MessageModel>();

    public connect() {
        if (!this.mConnectionCreated) {
            this.createConnection();
        } else {
            // Probably not necessary
            this.mStream.connect();
        }
    }

    public send({ id = uuid(), ...message }: MessageModel, expectReply: boolean = false): Observable<MessageModel | null> {
        const msgToSend = { ...message, id };
        Logger.debug(`Sending message with id: ${ id }`, message);
        return this.mConnectionStatus.pipe(
            filter((status: ConnectionStatus) => status === ConnectionStatus.connected),
            timeout(StreamClass.WAIT_FOR_CONNECTION_TIMEOUT),
            take(1),
            mergeMap(() => {
                Logger.debug(`Stream is connected...Sending message ${ msgToSend.id }`);
                this.mStream.emit('message', msgToSend);
                if (expectReply) {
                    return this.incomingMessages.pipe(
                        filter((msg) => msg.id === msgToSend.id),
                        timeout(StreamClass.WAIT_FOR_REPLY_TIMEOUT),
                        take(1),
                    );
                }
                return of(null);
            })
        );
    }

    public get status(): BehaviorSubject<ConnectionStatus> {
        return this.mConnectionStatus;
    }

    public get incomingMessages(): Subject<MessageModel> {
        return this.mIncomingMessages;
    }

    private createConnection() {
        this.mConnectionStatus.next(ConnectionStatus.initializing);
        Logger.debug(`Creating socket connection to: ${ this.streamUrl }`);
        this.mStream = io(this.streamUrl,{
            path: '/stream',
            transports: ['websocket'],
        });
        this.mStream.on('connect', () => {
            this.mConnectionStatus.next(ConnectionStatus.connected);
            Logger.debug('Stream connection established');
        });
        this.mStream.on('disconnect', (reason: string) => {
            this.mConnectionStatus.next(ConnectionStatus.disconnected);
            Logger.debug(`socket disconnected ${ reason }`);
        });
        this.mStream.on('message', (message: MessageModel) => {
            Logger.debug('Stream received message', message);
            this.mIncomingMessages.next(message);
        });
        this.mConnectionCreated = true;
    }
}

export const Stream = new StreamClass();