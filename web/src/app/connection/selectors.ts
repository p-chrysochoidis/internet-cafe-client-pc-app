import { AppState } from '../reducers';

export const connected = (state: AppState) => state.connection.connected;