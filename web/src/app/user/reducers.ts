import { UserStateModel } from './user-state.model';
import { handleActions } from 'redux-actions';

const emptyState: UserStateModel = {
    authenticated: false,
    authToken: null,
};

export const userReducer = handleActions({}, emptyState);
