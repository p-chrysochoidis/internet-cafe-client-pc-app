export interface UserStateModel {
    authenticated: boolean;
    authToken: string | null;
}