import React, { ChangeEvent, FormEvent, PureComponent } from 'react';
import { connect, MapStateToProps } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Redirect } from 'react-router-dom';

import { authenticated as authSelector } from '../selectors';
import { connected as connectedSelector } from '../../connection/selectors';
import { AppState } from '../../reducers';
import { Logger } from '../../utils/environment';
import { pcRegisteredSelector } from '../../client-pc/selectors';

type Props = StateToPros & DispatchProps;

interface StateToPros {
    authenticated: boolean;
    connected: boolean;
    pcRegistered: boolean;
}

interface DispatchProps {
}

interface InternalState {
    username: string;
    password: string;
    loginInProgress: boolean;
}

class LoginComponent extends PureComponent<Props, InternalState> {
    constructor(props: Props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loginInProgress: false,
        };
    }

    private updateState = (e: ChangeEvent) => {
        const target: any = e.target;
        const attr: any = target.getAttribute('name');
        const value: any = target.value;
        this.setState<any>({ [attr]: value });
    };

    private handleLoginFormSubmit = (e: FormEvent) => {
        e.preventDefault();
        Logger.debug('do login', e, this.state);
    };

    render() {
        const { authenticated, connected, pcRegistered } = this.props;
        const { username, password } = this.state;
        const loginButtonEnabled = !!(connected && username && password && pcRegistered);
        Logger.debug(connected, username, password, pcRegistered, loginButtonEnabled);
        if (authenticated) {
            return (
                <Redirect to="/" />
            );
        }
        return (
            <form className="login-component" onSubmit={ this.handleLoginFormSubmit }>
                <div className="form-group">
                    <label htmlFor="username-input">Username</label>
                    <input type="text"
                           className="form-control"
                           id="username-input"
                           name="username"
                           value={ username }
                           onChange={ this.updateState }
                           placeholder="Enter your username" />
                </div>
                <div className="form-group">
                    <label htmlFor="password-input">Password</label>
                    <input id="password-input"
                           type="password"
                           name="password"
                           className="form-control"
                           value={ password }
                           onChange={ this.updateState }
                           placeholder="Enter your password"
                    />
                </div>
                <button type="submit" className="btn btn-primary" disabled={ !loginButtonEnabled }>Login</button>
            </form>
        );
    }
}

const mapStateToProps: MapStateToProps<StateToPros, void, AppState> = createStructuredSelector({
    authenticated: authSelector,
    connected: connectedSelector,
    pcRegistered: pcRegisteredSelector,
});

export default connect(mapStateToProps)(LoginComponent);