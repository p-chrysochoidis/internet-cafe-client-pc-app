import { AppState } from '../reducers';

export const authenticated = (state: AppState) => state.user.authenticated;
export const authToken = (state: AppState) => state.user.authToken;