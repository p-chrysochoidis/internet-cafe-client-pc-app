const prefix = 'app.client_pc.';

export const clientPcActions = {
    startLoadingInfo: `${ prefix }info-start_loading`,
    keyAcquired: `${ prefix }key-acquired`,
    pcInfoLoaded: `${ prefix }pc_info-loaded`,
    clientInfoReady: `${ prefix }client_pc_info-ready`,
    clientPcRegistered: `${ prefix }client_pc_registered`,
};

export const clientPcStorageKeys = {
    clientPcKey: `${ prefix }client_pc.key`,
};