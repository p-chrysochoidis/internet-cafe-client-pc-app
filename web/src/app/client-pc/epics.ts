import { combineEpics, Epic, ofType } from 'redux-observable';
import { map, mapTo, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { combineLatest, of } from 'rxjs';
import { AnyAction } from 'redux';

import { clientPcActions } from './constants';
import ipcRenderer from '../electron/ipc-renderer';
import { acquireClientPcKey, clientPcInfoLoaded, clientPcInfoReady, clientPcRegistered } from './actions';
import { connectionActions } from '../connection/constants';
import { ConnectionStatus } from '../connection/connection-state';
import { Logger } from '../utils/environment';
import { RegisterClientPcModel } from '../connection/messages/outgoing/register-client-pc.model';
import { OutgoingMessageTypeEnum } from '../connection/messages/outgoing/outgoing-message-type.enum';
import { AppState } from '../reducers';
import { Stream } from '../connection/stream';

const pcInfoEpic: Epic = (action) => action.pipe(
    ofType<AnyAction, AnyAction, string>(clientPcActions.startLoadingInfo),
    mergeMap(() => ipcRenderer.getPcInfo()),
    map((pcInfo) => clientPcInfoLoaded(pcInfo)),
);

const pcKeyEpic: Epic = (action) => action.pipe(
    ofType<AnyAction, AnyAction, string>(clientPcActions.startLoadingInfo),
    mapTo(acquireClientPcKey())
);

const clientInfoReadyForConnectEpic: Epic = (action) => combineLatest(
    action.pipe(ofType<AnyAction, AnyAction, string>(clientPcActions.keyAcquired)),
    action.pipe(ofType<AnyAction, AnyAction, string>(clientPcActions.pcInfoLoaded)),
).pipe(
    mapTo(clientPcInfoReady())
);

const clientPcRegisterEpic: Epic = (action, state) => action.pipe(
    ofType<AnyAction, AnyAction, string>(connectionActions.statusChanged),
    map((statusAction: AnyAction) => statusAction.payload.status),
    tap((status: ConnectionStatus) => Logger.debug(`Socket status changed... ${ status }`)),
    withLatestFrom(state),
    switchMap((data: [ConnectionStatus, AppState]) => {
        const [status, state] = data;
        const { clientPc: { key, pcRegistered, ...restPcInfo }} = state;
        const pcKey: any = key;
        const pcInfo: any = restPcInfo;
        if (status === ConnectionStatus.connected) {
            const registerMsg: RegisterClientPcModel = {
                type: OutgoingMessageTypeEnum.registerClientPc,
                key: pcKey,
                ...pcInfo,
            };
            return Stream.send(registerMsg).pipe(mapTo(true));
        } else {
            return of(false);
        }
    }),
    tap((registered: boolean) => Logger.debug(`Client pc registered... ${ registered }`)),
    map((registered: boolean) => clientPcRegistered(registered))
);

export const clientPcEpic = combineEpics(
    pcInfoEpic,
    pcKeyEpic,
    clientInfoReadyForConnectEpic,
    clientPcRegisterEpic,
);