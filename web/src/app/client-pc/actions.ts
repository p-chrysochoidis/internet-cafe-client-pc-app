import { createAction } from 'redux-actions';
import uuid from 'uuid/v4';

import { clientPcActions, clientPcStorageKeys } from './constants';
import { Logger } from '../utils/environment';
import { PcInfoModel } from '../electron/pc-info.model';

export const startLoadingPcInfo = createAction(clientPcActions.startLoadingInfo);
export const acquireClientPcKey = createAction(clientPcActions.keyAcquired, () => {
    let key = localStorage.getItem(clientPcStorageKeys.clientPcKey);
    Logger.debug(`Client pc key: ${ key }`);
    if (!key) {
        key = uuid();
        Logger.debug(`Created new client pc key: ${ key }`);
        try {
            localStorage.setItem(clientPcStorageKeys.clientPcKey, key);
        } catch (e) {
            Logger.error(`Saving key failed`, e);
        }
    }
    return { key };
});
export const clientPcInfoLoaded = createAction(
    clientPcActions.pcInfoLoaded,
    (pcInfo: PcInfoModel) => pcInfo
);
export const clientPcInfoReady = createAction(clientPcActions.clientInfoReady);
export const clientPcRegistered = createAction(clientPcActions.clientPcRegistered, (registered: boolean) =>
    ({ registered })
);