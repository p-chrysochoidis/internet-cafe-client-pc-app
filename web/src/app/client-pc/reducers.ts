import { handleActions } from 'redux-actions';
import { AnyAction } from 'redux';

import { ClientPcStateModel } from './client-pc-state.model';
import { clientPcActions } from './constants';

const emptyState: ClientPcStateModel = {
    key: null,
    pcIp: null,
    pcName: null,
    pcOsType: null,
    pcOsArch: null,
    pcOsRelease: null,
    pcUserName: null,
    pcRegistered: false,
};

export const clientPcReducer = handleActions({
    [clientPcActions.keyAcquired]: (state: ClientPcStateModel, { payload: { key } }: AnyAction) => ({ ...state, key }),
    [clientPcActions.pcInfoLoaded]: (state: ClientPcStateModel, { payload: { ...pcInfo } }: AnyAction) =>
        ({ ...state, ...pcInfo }),
    [clientPcActions.clientPcRegistered]: (state: ClientPcStateModel, { payload: { registered } }: AnyAction) =>
        ({ ...state, pcRegistered: registered }),
}, emptyState);