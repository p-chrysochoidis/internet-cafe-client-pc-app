export interface ClientPcStateModel {
    key: string | null,
    pcName: string | null,
    pcIp: string | null,
    pcOsArch: string | null,
    pcOsType: string | null,
    pcOsRelease: string | null,
    pcUserName: string | null,
    pcRegistered: boolean,
}