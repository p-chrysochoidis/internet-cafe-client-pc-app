import { AppState } from '../reducers';

export const pcRegisteredSelector = (state: AppState) => state.clientPc.pcRegistered;