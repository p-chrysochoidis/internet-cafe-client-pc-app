const prefix = 'app.background-images';

export const backgroundImagesActions = {
    loadImages: `${ prefix }.load-images`,
    loadedImages: `${ prefix }.loaded-images`,
    showImage: `${ prefix }.show-image`,
};