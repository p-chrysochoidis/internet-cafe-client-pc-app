import { combineEpics, Epic, ofType } from 'redux-observable';
import { AnyAction } from 'redux';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

import { backgroundImagesActions } from './constants';
import { Stream } from '../connection/stream';
import { LoadBackgroundImagesModel } from '../connection/messages/outgoing/load-background-images.model';
import { OutgoingMessageTypeEnum } from '../connection/messages/outgoing/outgoing-message-type.enum';
import { LoadedBackgroundImagesModel } from '../connection/messages/incoming/loaded-background-images.model';
import { loadedImages } from './actions';
import { MessageModel } from '../connection/messages/message.model';

const loadImagesEpic: Epic = (action) => action.pipe(
    ofType<AnyAction, AnyAction, string>(backgroundImagesActions.loadImages),
    switchMap(() => {
        const msg: LoadBackgroundImagesModel = { type: OutgoingMessageTypeEnum.loadBackgroundImages };
        return Stream.send(msg, true).pipe(
            // TODO: handle error?
            catchError(() => of(null) )
        );
    }),
    map((msg: MessageModel | null) => msg as LoadedBackgroundImagesModel),
    map((imagesMsg: LoadedBackgroundImagesModel) => imagesMsg && imagesMsg.images),
    map((images: string[]) => loadedImages(images || []))
);

export const backgroundImagesEpic = combineEpics(loadImagesEpic);