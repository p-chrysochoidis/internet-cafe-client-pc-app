import React, { PureComponent } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { loadImages } from './actions';
import { visibleImageSrcSelector } from './selectors';

import './background-images.component.scss';
import { Logger } from '../utils/environment';
import { AppState } from '../reducers';

interface StateToProps {
    visibleImageSrc: string | null;
}

interface DispatchToProps {
    loadImages: () => void;
}

type Props = StateToProps & DispatchToProps;

class BackgroundImagesComponent extends PureComponent<Props> {
    componentDidMount(): void {
        this.props.loadImages();
    }

    render() {
        const { visibleImageSrc } = this.props;
        let style: any = {};
        Logger.debug(`visible image src: ${ visibleImageSrc }`);
        if (visibleImageSrc) {
            style.backgroundImage = `url('${ visibleImageSrc }')`;
        }
        return (
            <div className="background-container" style={ style } />
        );
    }
}

const mapStateToProps: MapStateToProps<StateToProps, void, AppState> = createStructuredSelector({
    visibleImageSrc: visibleImageSrcSelector,
});

const mapDispatchToProps: MapDispatchToProps<DispatchToProps, void> = {
    loadImages,
};

export default connect(mapStateToProps, mapDispatchToProps)(BackgroundImagesComponent);