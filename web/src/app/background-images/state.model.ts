export interface BackgroundImagesStateModel {
    images: string[];
    visibleImageIndex: number;
}
