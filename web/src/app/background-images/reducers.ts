import { handleActions } from 'redux-actions';

import { BackgroundImagesStateModel } from './state.model';
import { backgroundImagesActions } from './constants';
import { AnyAction } from 'redux';
import { Logger } from '../utils/environment';

const emptyState: BackgroundImagesStateModel = {
    images: [],
    visibleImageIndex: -1,
};

export const backgroundImagesReducer = handleActions({
    [backgroundImagesActions.loadImages]: (state: BackgroundImagesStateModel) =>
        ({ ...state, images: [], visibleImageIndex: -1}),
    [backgroundImagesActions.showImage]: (state: BackgroundImagesStateModel, { payload: imageIndex }: AnyAction) => {
        let index = imageIndex;
        if (index >= state.images.length) {
            index = state.images.length > 0 ? 0 : -1;
        }
        return { ...state, visibleImageIndex: index };
    },
    [backgroundImagesActions.loadedImages]: (state: BackgroundImagesStateModel, { payload: { images } }: AnyAction) => {
        Logger.debug('Images loaded', state, images);
        return { ...state, images, visibleImageIndex: 0 };
    },
}, emptyState);
