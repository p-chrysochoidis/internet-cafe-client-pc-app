import { createAction } from 'redux-actions';

import { backgroundImagesActions } from './constants';

export const loadImages = createAction(backgroundImagesActions.loadImages);
export const loadedImages = createAction(backgroundImagesActions.loadedImages, (images: string[]) => ({ images }));
export const showImage = createAction(backgroundImagesActions.showImage, (imageIndex: number) => ({ index: imageIndex }));
