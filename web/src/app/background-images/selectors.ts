import { createSelector } from 'reselect';
import { AppState } from '../reducers';
import { Logger } from '../utils/environment';

const backgroundImagesSelector = (state: AppState) => (state.backgroundImages.images);
const visibleImageIndexSelector = (state: AppState) => (state.backgroundImages.visibleImageIndex);
export const visibleImageSrcSelector = createSelector(
    backgroundImagesSelector,
    visibleImageIndexSelector,
    (images, index) => {
        Logger.debug('visibleImageSrcSelector', images, index, images.length);
        if (!images || !images.length || index < 0 || index >= images.length) {
            return null;
        }
        return images[index];
    },
);