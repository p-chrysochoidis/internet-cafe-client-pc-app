import React, { PureComponent } from 'react';
import { Redirect, Route, RouteProps } from 'react-router';
import { MapStateToProps } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import { AppState } from '../../reducers';
import { authenticated } from '../../user/selectors';

interface MappedProps {
    authenticated: boolean,
}

type ComponentProps = MappedProps & RouteProps;

class AuthenticatedRouteComponent extends PureComponent<ComponentProps> {
    render() {
        const {authenticated, ...props} = this.props;
        let newProps = {...props};
        if (!authenticated) {
            delete newProps['component'];
            newProps['render'] = () => (<Redirect to="/" />);
        }
        return (<Route { ...newProps } />);
    }
}

const mapStateToProps: MapStateToProps<MappedProps, void, AppState> = createStructuredSelector({
    authenticated: authenticated,
});

export default connect(mapStateToProps)(AuthenticatedRouteComponent);