import * as log from 'loglevel';
import { Store } from 'redux';

import { NodeEnvEnum } from './node-env.enum';

export class Environment {
    private static readonly env: string = process.env.NODE_ENV;
    private static loggerInitialized: boolean = false;

    public static getLogger() {
        if (!Environment.loggerInitialized) {
            if (Environment.isDevelopment()) {
                log.enableAll();
            } else {
                log.disableAll();
            }
            Environment.loggerInitialized = true;
        }
        return log;
    }

    public static isDevelopment(): boolean {
        return Environment.env === NodeEnvEnum.dev;
    }

    public static isProduction(): boolean {
        return Environment.env === NodeEnvEnum.prod;
    }

    public static setupDev(appStore: Store): void {
        log.debug('Checking env to install dev tools');
        if (Environment.isDevelopment()) {
            Environment.setupDevTools(appStore);
        } else {
            log.debug('Not in dev. Skipping devtools setup');
        }
    }

    private static setupDevTools(appStore: Store): void {
        log.info('Setting up dev tools');

        let w = window as any;
        if (w) {
            log.info('Store available under window.store');
            w.store = appStore;
        }
    }
}

export const Logger = Environment.getLogger();