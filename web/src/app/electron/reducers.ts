import { handleActions } from 'redux-actions';

import { actions } from './constants';
import { PcStatusEnum } from './pc-status.enum';

export interface ElectronState {
    status: PcStatusEnum;
}

const emptyState: ElectronState = {
    status: PcStatusEnum.on,
};

export const electronReducer = handleActions({
        [actions.reboot]: (state) => ({...state, status: PcStatusEnum.rebooting}),
    },
    emptyState,
);