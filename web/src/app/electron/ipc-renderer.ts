import { fromEvent, Observable } from 'rxjs';
import { filter, map, take, timeout } from 'rxjs/operators';
import uuid from 'uuid/v4';

import { PcInfoModel } from './pc-info.model';
import { IpcMessageModel } from './ipc-messages/ipc-message.model';
import { IpcMessageTypeEnum } from './ipc-messages/ipc-message-type.enum';
import { IPC_MESSAGE_TIMEOUT_MS } from './constants';
import { ClientPcInfoMsgModel } from './ipc-messages/from-main/client-pc-info-msg.model';

let ipcRenderer = nodeRequire('electron').ipcRenderer;

class IpcRenderer {
    private ipc = ipcRenderer;
    private messagesFromIpcMain: Observable<IpcMessageModel>;

    constructor() {
        this.messagesFromIpcMain = fromEvent<[any, IpcMessageModel]>(ipcRenderer, 'msg')
            .pipe(map((data) => data[1]));
    }

    public getPcInfo(): Observable<PcInfoModel> {
        const msg = { id: uuid(), type: IpcMessageTypeEnum.clientPcInfo };
        this.ipc.send('msg', msg);
        return this.messagesFromIpcMain.pipe(
            filter((incomingMsg) => msg.id === incomingMsg.id && incomingMsg.type === msg.type),
            take(1),
            timeout(IPC_MESSAGE_TIMEOUT_MS*5),
            map((incoming) => {
                const { id, type, ...pcInfo } = <ClientPcInfoMsgModel> incoming;
                return { ...pcInfo };
            }),
        );
    }

    public setAlwaysOnTop(alwaysOnTop: boolean): void {
        this.ipc.send("window-always-on-top", alwaysOnTop);
    }

    public setFullScreen(fullScreen: boolean): void {
        this.ipc.send("window-fullscreen", fullScreen);
    }

    public reboot(): void {
        console.log('Rebooting...');
        this.ipc.send('reboot');
    }
}
const renderer = new IpcRenderer();
export default renderer;