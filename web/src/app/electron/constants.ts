const prefix = 'app.electron.';

export const actions = {
    reboot: `${ prefix }reboot`,
};

export const IPC_MESSAGE_TIMEOUT_MS = 1000;