import { IpcMessageTypeEnum } from './ipc-message-type.enum';

export interface IpcMessageModel {
    id: string,
    type: IpcMessageTypeEnum,
}