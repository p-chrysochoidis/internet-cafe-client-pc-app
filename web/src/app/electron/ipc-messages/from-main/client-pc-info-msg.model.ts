import { IpcMessageModel } from '../ipc-message.model';
import { PcInfoModel } from '../../pc-info.model';

export interface ClientPcInfoMsgModel extends IpcMessageModel, PcInfoModel { }