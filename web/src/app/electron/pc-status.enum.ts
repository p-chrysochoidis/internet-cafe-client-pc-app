export enum PcStatusEnum {
    on,
    rebooting,
    shuttingDown,
}