export interface PcInfoModel {
    pcName: string,
    pcIp: string,
    pcOsArch: string,
    pcOsType: string,
    pcOsRelease: string,
    pcUserName: string,
}