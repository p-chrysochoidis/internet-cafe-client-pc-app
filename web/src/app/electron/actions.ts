import { createAction } from 'redux-actions';

import { actions } from './constants';
import IpcRenderer from './ipc-renderer';

export const rebootAction = createAction(actions.reboot, () => IpcRenderer.reboot());