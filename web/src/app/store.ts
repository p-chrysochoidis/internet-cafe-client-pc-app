import { createEpicMiddleware } from 'redux-observable';
import { applyMiddleware, createStore } from 'redux';

import { rootReducer } from './reducers';
import { rootEpic } from './epics';

const epicMiddleware = createEpicMiddleware();

export const appStore = createStore(rootReducer, applyMiddleware(epicMiddleware));

epicMiddleware.run(rootEpic);