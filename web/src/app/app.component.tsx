import React, { PureComponent } from 'react';
import { connect, MapDispatchToProps, MapStateToProps } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

import { AppState } from './reducers';
import { rebootAction } from './electron/actions';
import { Logger } from './utils/environment';
import { startLoadingPcInfo } from './client-pc/actions';
import BackgroundImagesComponent from './background-images/background-images.component';
import LoginComponent from './user/login/login.component';
import AuthenticatedRoute from './utils/authenticated-route/authenticated-route.component';
import { pcRegisteredSelector } from './client-pc/selectors';

import './app.component.scss';

type AppProps = AppStateToPros & AppDispatchProps;

interface AppStateToPros {
    clientPcRegistered: boolean,
}

interface AppDispatchProps {
    reboot: () => void,
    startLoadingPcInfo: () => void,
}

interface AppInternalState {
    backgroundVisible: boolean;
}

class AppComponent extends PureComponent<AppProps, AppInternalState> {
    constructor(props: AppProps) {
        super(props);
        this.state = { backgroundVisible: false };
    }

    static getDerivedStateFromProps(props: AppProps, state: AppInternalState) {
        return { ...state, backgroundVisible: props.clientPcRegistered || state.backgroundVisible };
    }

    componentDidMount(): void {
        this.props.startLoadingPcInfo();
    }

    private reboot = (): void => {
        this.props.reboot();
    };

    render() {
        Logger.debug('Render App', this.props);
        const {
            backgroundVisible,
        } = this.state;
        return (
            <div className="app-component">
                { backgroundVisible && (<BackgroundImagesComponent />) }
                <div className="app-component__content">
                    <BrowserRouter>
                        <Link to="/login">Login</Link>
                        <Link to="/user/">User</Link>
                        <Switch >
                            <Route path='/login' component={ LoginComponent }/>
                            <AuthenticatedRoute exact path="/user" render={() => (<h1>Auth route</h1>)} />
                        </Switch>
                    </BrowserRouter>
                </div>
            </div>
        );
    }
}

const mapStateToProps: MapStateToProps<AppStateToPros, void, AppState> = createStructuredSelector({
    clientPcRegistered: pcRegisteredSelector,
});

const mapDispatchToProps: MapDispatchToProps<AppDispatchProps, void> = {
    reboot: rebootAction,
    startLoadingPcInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppComponent);
