import { app, BrowserWindow, protocol } from 'electron';
import { format } from 'url';
import { ipcMain, globalShortcut } from 'electron';
import { join, normalize } from 'path';
import { exec } from 'child_process';
import { address } from 'ip';
import { hostname, arch, type, release, userInfo } from 'os';

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let allowQuit = false;

console.log('Starting app');

const isDevelopment = process.env.NODE_ENV === 'development';

const htmlPath = isDevelopment ?
    'http://localhost:3000' :
    format({
        protocol: 'app:',
        pathname: './index.html',
        slashes: true,
    });

function createWindow() {
    // Create the browser window.
    //TODO: move to config
    mainWindow = new BrowserWindow({
        width: 400,
        height: 250,
        minWidth: 200,
        minHeight: 125,
        alwaysOnTop: false,
        fullscreen: false,
        backgroundColor: '#CECCCC',
        title: 'Internet Cafe Client',
        center: true,
        webPreferences: {
            nodeIntegration: true,
            devTools: true,
        },
        titleBarStyle: 'hidden',
        autoHideMenuBar: true,
        icon: join(__dirname, '../src/favicon.png'),
    });
    mainWindow.setMenu(null);
    console.log(`Html path: ${htmlPath}`);

    // and load the index.html of the app.
    mainWindow.loadURL(htmlPath);

    if (isDevelopment) {
        // Open the DevTools.
        mainWindow.webContents.openDevTools({mode: 'undocked'});
    }

    mainWindow.on('beforeUnload', (e) => {
        console.log('I do not want to be closed');

        // Unlike usual browsers that a message box will be prompted to users, returning
        // a non-void value will silently cancel the close.
        // It is recommended to use the dialog API to let the user confirm closing the
        // application.
        e.returnValue = false; // equivalent to `return false` but not recommended
    });

    mainWindow.on('close', function (event) {
        console.log('window close', event);
        // event.preventDefault();
    });

    mainWindow.on('blur', function (event) {
        // mainWindow.focus();
    });

    mainWindow.on('focus', function (event) {

    });

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
    globalShortcut.register('Ctrl+Alt+Delete', () => {
        console.log('Ctrl+Alt+Delete');
    });
    protocol.registerFileProtocol('app', (request, callback) => {
        const url = request.url.substr(7);
        callback(normalize(`${__dirname}/${url}`));
    }, (error) => {
        if (error) console.error('Failed to register protocol');
    });
    if (isDevelopment) {
        import('electron-devtools-installer').then(({ default: electronDevToolsInstall, REACT_DEVELOPER_TOOLS}) => {
            return electronDevToolsInstall(REACT_DEVELOPER_TOOLS);
        })
            .then(() => console.log('React dev tools installed'))
    }
    createWindow();
});

app.on('before-quit', (event) => {
    console.log('before-quit', event);
    // event.preventDefault();
});

app.on('will-quit', (event) => {
    console.log('will-quit', event);
    // event.preventDefault();
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
});

ipcMain.on('window-always-on-top', (event, args) => {
    if (mainWindow) {
        mainWindow.setAlwaysOnTop(args);
    }
});
ipcMain.on('window-fullscreen', (event, args) => {
    if (mainWindow) {
        mainWindow.setFullScreen(args);
    }
});
ipcMain.on('msg', (event, args) => {
    console.log('allow quit', args);
    let responseMsg = null;
    switch (args.type) {
        case 'client-pc-info':
            responseMsg = {
                type: args.type,
                id: args.id,
                pcIp: address(),
                pcName: hostname(),
                pcOsArch: arch(),
                pcOsType: type(),
                pcOsRelease: release(),
                pcUserName: userInfo().username,
            };
            break;
    }
    if (responseMsg) {
        console.log(responseMsg);
        event.sender.send('msg', responseMsg);
    }
});

ipcMain.on('reboot', (event, args) => {
    console.log('reboot', args);
    //TODO:
    if (process.platform === 'win32') {
        exec('shutdown /r /f /t 0');
    } else {
        console.log(`${process.platform} is not supported`);
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.