#!/bin/bash

BASEDIR=$(dirname "$0")
ROOT=$(cd $BASEDIR && cd ../ && pwd)
IMAGE_NAME="internet-cafe-client-pc-app-development"

docker run -it --rm -v $ROOT:/app -v /tmp/.X11-unix:/tmp/.X11-unix --user $(id -u) -v /dev/shm:/dev/shm $IMAGE_NAME
