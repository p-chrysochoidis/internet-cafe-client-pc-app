#!/bin/bash

BASEDIR=$(dirname "$0")
ROOT=$(cd $BASEDIR && cd ../ && pwd)
IMAGE_NAME="internet-cafe-client-pc-app-development"

docker build -t $IMAGE_NAME -f "$ROOT/docker/dev-image/Dockerfile" "$ROOT/docker/dev-image"
